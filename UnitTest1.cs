using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Jenkins_Practice
{
    public class Tests
    {
        private IWebDriver _driver;
        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
        }

        [Test]
        public void Test1()
        {
            _driver.Url = "https://www.flipkart.com/";
            _driver.FindElement(By.XPath("//button[@class='_2KpZ6l _2doB4z']")).Click();
            IWebElement search = _driver.FindElement(By.XPath("//input[@name='q']"));
            search.SendKeys("Mobiles" + Keys.Enter);
            //Assert.Fail();
            Assert.Pass();
            

        }
        [TearDown] public void Teardown()
        {
            _driver.Close();
        }
    }
}